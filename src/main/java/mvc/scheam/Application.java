package mvc.scheam;

import java.io.IOException;

/**
 * Entry point in my home task
 * That is the last task in the list;
 *
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public class Application {
    public static void main(final String[] args) {
        Controller l = new Controller();
        try {
            l.start();
        } catch (IOException ioe) {
            ioe.printStackTrace(System.out);
        }
    }
}
