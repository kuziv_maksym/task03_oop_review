package mvc.scheam;

import java.util.List;
import data.units.Decoration;
/**
 * It is necessary to use MVC scheme in  this home task<br>
 * This class is required by this scheme<br>
 * This is abstract class presenting Model
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public abstract class Model {
    /**
     * Sorts data
     */
    public abstract void sort();
    /**
     * Searching for Decorations by concrete type<br>
     *
     * @return result List of Decorations by concrete type
     */
    public abstract List<Decoration> searchByDT();

    /**
     * Creates Decorations using Object DecorationGenerator
     *
     * @param quantity of Decoration Objects to generate
     */
    public abstract void createDecorations(int quantity);
}
