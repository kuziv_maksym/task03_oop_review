package mvc.scheam;
/**
 * It is necessary to use MVC scheme in  this home task<br>
 * This class is required by this scheme<br>
 * This class display menu and result of user's actions
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public class View {
    /**
     * Display given text on the screen
     *
     * @param text displays text on the screen
     */
    public void displayText(final String text) {
        System.out.println(text);
    }
}
