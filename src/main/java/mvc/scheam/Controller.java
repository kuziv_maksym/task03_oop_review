package mvc.scheam;

import data.units.Decoration;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.IOException;

/**
 * It is necessary to use MVC scheme in  this home task<br>
 * This class is required by this scheme<br>
 * User interact with this class, to make some user's calculations<br>
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public class Controller extends Model {
    /**
     * This is the menu user work with
     */
    private String mainText =
            "Please select option:\n"
                    +
            "0) quit\n"
                    +
            "1) generate decorations\n"
                    +
            "2) sort decorations\n"
                    +
            "3) display decorations\n"
                    +
            "4) search by";
    /**
     * This is menu for selection different DecorTypes
     */
    private String selectText =
            "Please select Decoration types"
                    +
            "You want to see:\n"
                    +
            "0) display\n"
                    +
            "1) Car decoration\n"
                    +
            "2) Home decoration\n"
                    +
            "3) Tree decoration";
    /**
     * To work with data, stored in Class Data<br>
     * See also {@link package mvn.scheam#member Data}
     */
    private Data data = new Data();
    /**
     * To display menu and output on the screen<br>
     */
    private View mainView = new View();
    /**
     * To organize interaction between program and user
     */
    private Scanner scan = new Scanner(System.in);

    /**
     * Sort list in Data object
     */
    @Override
    public void sort() {
        data.sort();
    }

    /**
     * Searches for Decorations which has special Decortypes<br>
     * User can also select all the types, in this case<br>
     * That is the same as sort main storage
     *
     * @return result List with all selected DecorTypes<br>
     * sorted according to their priority
     */
    @Override
    public List<Decoration> searchByDT() {
        List<Decoration> originData = data.getData();
        ArrayList<Decoration> result = new ArrayList<Decoration>();
        ArrayList<Integer> typesArray = new ArrayList<Integer>();
        mainView.displayText(selectText);
        String c = scan.next("[0-3]");
        int tmp = 1;
        while (!c.equals("0") && tmp < 3) {
            typesArray.add(Integer.parseInt(c));
            tmp++;
            mainView.displayText(selectText);
            c = scan.next();
        }
        for (Integer i : typesArray) {
            for (Decoration d : originData) {
                if (d.getDecorType() == i) {
                    result.add(d);
                }
            }
        }
        result.sort(null);
        for (Decoration d : result) {
            System.out.println(d + "\n");
        }
        if (result.size() == 0) {
            mainView.displayText("You didn't choose anything!\n");
            searchByDT();
        }
        return result;
    }

    /**
     * Generates Decorations.
     *
     * @param quantity of Decorations user want to have
     */
    public void createDecorations(final int quantity) {
        data.createDecorations(quantity);
    }

    /**
     * To generate output of content of main storage of Decorations
     *
     * @return String representing the content of the main storage
     */
    public String generateView() {
        StringBuilder result = new StringBuilder();
        for (Decoration d : data.getData()) {
            mainView.displayText(d.toString());
        }
        return result.toString();
    }

    /**
     * Main loop of my home task, throws IOException
     */
    public void start() throws IOException {
        mainView.displayText(mainText);
        String c = scan.next("[0-5]");
        while (!c.equals("0")) {
            switch (c) {
                case "0" : {
                    return;
                }
                case "1" : {
                    System.out.println("Please insert quantity:");
                    int quantity = scan.nextInt();
                    createDecorations(quantity);
                    break;
                }
                case "2" : {
                    sort();
                    break;
                }
                case "3" : {
                    mainView.displayText(generateView());
                    break;
                }
                case "4" : {
                    searchByDT();
                }
                default : {
                    System.out.println("Please insert numbers in range[0-4]!");
                    break;
                }
            }
            mainView.displayText(mainText);
            c = scan.next("[0-5]");
        }
    }
}
