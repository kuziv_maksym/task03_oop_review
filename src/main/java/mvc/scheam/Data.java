package mvc.scheam;
import java.util.*;
import data.units.*;

/**
 * It is necessary to use MVC scheme in  this home task<br>
 * This class is required by this scheme<br>
 * This class works with main data
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public class Data extends Model {
    /**
     * Random is used to randomly generate Decorations
     */
    private Random rand = new Random(47);
    /**
     * List of decorations(main data)
     */
    private List<Decoration> decorList = new ArrayList<Decoration>();
    /**
     * Generator of Decorations.
     * See also {@link package data.units#member Decoration}
     */
    private DecorationGenerator gc = new DecorationGenerator();
    @Override
    /**
     * Sorts main data using method List.sort
     */
    public void sort() {
        decorList.sort(null);
    }
    /**
     * Implements Searching for Decorations by concrete type<br>
     *
     * @return result List of Decorations by concrete type
     */
    @Override
    public List<Decoration> searchByDT() {
        return null;
    }
    /**
     * Creates Decorations using Object DecorationGenerator<br>
     * Automatically inserts Decorations in List of main data
     *
     * @param quantity of Decoration Objects to generate
     */
    @Override
    public void createDecorations(final int quantity) {
        for (int i = 0; i < quantity; i++) {
            decorList.add(gc.generateDecor());
        }
    }
    /**
     * Returns list of main data
     *
     * @return List of main Data
     */
    public List<Decoration> getData() {
        return decorList;
    }
}
