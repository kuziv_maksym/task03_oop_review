package data.units;
import decor.units.DecorType;

/**
 * This class presents New-Year Decoration: Ball
 * @version 0.1
 * @author Andriy Mota
 * @since 2019.11.10
 */
public class Ball extends Decoration {
    /**
     * State which decoration type is this decoration
     */
    private DecorType decorType;
    /**
     *
     * @param color set color for decoration
     * @param location set location for decoration
     * @param decorType set decorType for decoration
     */
    public Ball(final String color, final String location,
                final DecorType decorType) {
        super(color, location, decorType);
        this.decorType = decorType;
    }
}
