package decor.units;

/**
 * Abstract class representing Type New-Year Decorations<br>
 * To compare two DecorTypes field priority is used<br>
 * To provide ability to use Decorations in List, and use sort method,<br>
 * DecorType implements Comparable.
 *
 * @author Andriy Mota
 * @version 0.1
 * @since 2019.11.10
 */
public abstract class DecorType implements Comparable<DecorType> {
    /**
     * type of decoration
     */
    private String type;
    /**
     * Priority, which is used to sort elements is List<br>
     * Max priority = 1, min depends on value of decorTypes<br>
     * In my case min = 3
     */
    private int priority;

    /**
     * Sets priority to the DecorType object
     * @param i priority of the DecorType object
     */
    public DecorType(final int i) {
        priority = i;
    }

    /**
     * Compare to DecorTypes by their priority;
     *
     * @param o DecorType object to compare with
     * @return 0 if original object has bigger priority than o;<br>
     * -1 if original object has bigger priority than o
     */
    public int compareTo(final DecorType o) {
        return priority < o.priority ? -1 : 0;
    }
    /**
     * @return String representation of CarDecor priority
     */
    public String toString() {
        return type;
    }

    /**
     * @return priority of current DecorType
     */
    public int getPriority() {
        return priority;
    }
}
